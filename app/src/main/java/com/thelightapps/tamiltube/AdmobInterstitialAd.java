package com.thelightapps.tamiltube;

/**
 * Created by Lenovo on 8/13/2017.
 */

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import android.content.Context;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.purchase.InAppPurchaseListener;
import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.internal.zzdt;
import com.google.android.gms.internal.zzfa;

public final class AdmobInterstitialAd {
    private final zzfa zzrL;

    public AdmobInterstitialAd(Context var1) {
        this.zzrL = new zzfa(var1);
    }

    public AdListener getAdListener() {
        return this.zzrL.getAdListener();
    }

    public String getAdUnitId() {
        return this.zzrL.getAdUnitId();
    }

    public InAppPurchaseListener getInAppPurchaseListener() {
        return this.zzrL.getInAppPurchaseListener();
    }

    public boolean isLoaded() {
        return this.zzrL.isLoaded();
    }

    public boolean isLoading() {
        return this.zzrL.isLoading();
    }

    @RequiresPermission("android.permission.INTERNET")
    public void loadAd(AdRequest var1) {
        this.zzrL.zza(var1.zzbq());
    }

    public void setAdListener(AdListener var1) {
        this.zzrL.setAdListener(var1);
        if(var1 != null && var1 instanceof zzdt) {
            this.zzrL.zza((zzdt)var1);
        } else if(var1 == null) {
            this.zzrL.zza((zzdt)null);
        }

    }

    public void setAdUnitId(String var1) {
        this.zzrL.setAdUnitId(var1);
    }

    public void setInAppPurchaseListener(InAppPurchaseListener var1) {
        this.zzrL.setInAppPurchaseListener(var1);
    }

    public void setPlayStorePurchaseParams(PlayStorePurchaseListener var1, String var2) {
        this.zzrL.setPlayStorePurchaseParams(var1, var2);
    }

    public String getMediationAdapterClassName() {
        return this.zzrL.getMediationAdapterClassName();
    }

    public void show() {
        this.zzrL.show();
    }

    public void setRewardedVideoAdListener(RewardedVideoAdListener var1) {
        this.zzrL.setRewardedVideoAdListener(var1);
    }

    public void zzd(boolean var1) {
        this.zzrL.zzd(var1);
    }
}

