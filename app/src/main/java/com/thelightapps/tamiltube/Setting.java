package com.thelightapps.tamiltube;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Telephony;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class Setting extends AppCompatActivity {

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    String myTheme;
    Context context = null;
    RelativeLayout theme1, theme2, theme3, theme4, theme5, theme6, theme7, theme8, theme9, theme10, theme11, theme12, theme13,
            theme14, theme15, theme16, theme17;
    TextView txtTheme1, txtTheme2, txtTheme3, txtTheme4, txtTheme5, txtTheme6, txtTheme7, txtTheme8, txtTheme9, txtTheme10,
            txtTheme11, txtTheme12, txtTheme13, txtTheme14, txtTheme15, txtTheme16, txtTheme17;
    SharedPreferences prefs;

    public static final int[] ArrayImage = {
            R.drawable.home, R.drawable.category,
            R.drawable.ic_fiber_new_black_24dp,R.drawable.ic_favorite_black_24dp,
            R.drawable.trending, R.drawable.like,
            R.drawable.share,
            R.drawable.upload, R.drawable.theme,
            R.drawable.app_share, R.drawable.more_app, R.drawable.rate};
    Typeface tf;
    View layout12;
    android.support.v7.app.ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tf = Typeface.createFromAsset(Setting.this.getAssets(), "fonts/Raleway-Light.ttf");

        prefs = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE);
        myTheme = prefs.getString("Color", null);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Setting.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (myTheme != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            actionBar = getSupportActionBar();
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayOptions(actionBar.getDisplayOptions() | ActionBar.DISPLAY_SHOW_CUSTOM);

            TextView title = new TextView(actionBar.getThemedContext());
            title.setTextColor(Color.WHITE);
            title.setGravity(Gravity.RIGHT);
            title.setText(R.string.menuTheme);
            title.setTextSize(18.0f);
            title.setTypeface(tf);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            title.setPadding(100, 0, 0, 0);
            title.setWidth(width);

            actionBar.setCustomView(title);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(myTheme)));

        } else {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            actionBar = getSupportActionBar();
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayOptions(actionBar.getDisplayOptions() | ActionBar.DISPLAY_SHOW_CUSTOM);

            TextView title = new TextView(actionBar.getThemedContext());
            title.setTextColor(Color.WHITE);
            title.setGravity(Gravity.RIGHT);
            title.setText(R.string.menuTheme);
            title.setTypeface(tf);
            title.setTextSize(18.0f);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            title.setPadding(100, 0, 0, 0);
            title.setWidth(width);

            actionBar.setCustomView(title);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f7412c")));

        }

        setContentView(R.layout.activity_setting);

        mDrawerList = (ListView) findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        addDrawerItems();
        setupDrawer();

        txtTheme1 = (TextView) findViewById(R.id.txtTheme1);
        txtTheme2 = (TextView) findViewById(R.id.txtTheme2);
        txtTheme3 = (TextView) findViewById(R.id.txtTheme3);
        txtTheme4 = (TextView) findViewById(R.id.txtTheme4);
        txtTheme5 = (TextView) findViewById(R.id.txtTheme5);
        txtTheme6 = (TextView) findViewById(R.id.txtTheme6);
        txtTheme7 = (TextView) findViewById(R.id.txtTheme7);
        txtTheme8 = (TextView) findViewById(R.id.txtTheme8);
        txtTheme9 = (TextView) findViewById(R.id.txtTheme9);
        txtTheme10 = (TextView) findViewById(R.id.txtTheme10);
        txtTheme11 = (TextView) findViewById(R.id.txtTheme11);
        txtTheme12 = (TextView) findViewById(R.id.txtTheme12);
        txtTheme13 = (TextView) findViewById(R.id.txtTheme13);
        txtTheme14 = (TextView) findViewById(R.id.txtTheme14);
        txtTheme15 = (TextView) findViewById(R.id.txtTheme15);
        txtTheme16 = (TextView) findViewById(R.id.txtTheme16);
        txtTheme17 = (TextView) findViewById(R.id.txtTheme17);

        txtTheme1.setTypeface(tf);
        txtTheme2.setTypeface(tf);
        txtTheme3.setTypeface(tf);
        txtTheme4.setTypeface(tf);
        txtTheme5.setTypeface(tf);
        txtTheme6.setTypeface(tf);
        txtTheme7.setTypeface(tf);
        txtTheme8.setTypeface(tf);
        txtTheme9.setTypeface(tf);
        txtTheme10.setTypeface(tf);
        txtTheme11.setTypeface(tf);
        txtTheme12.setTypeface(tf);
        txtTheme13.setTypeface(tf);
        txtTheme14.setTypeface(tf);
        txtTheme15.setTypeface(tf);
        txtTheme16.setTypeface(tf);
        txtTheme17.setTypeface(tf);

        theme1 = (RelativeLayout) findViewById(R.id.theme1);
        theme2 = (RelativeLayout) findViewById(R.id.theme2);
        theme3 = (RelativeLayout) findViewById(R.id.theme3);
        theme4 = (RelativeLayout) findViewById(R.id.theme4);
        theme5 = (RelativeLayout) findViewById(R.id.theme5);
        theme6 = (RelativeLayout) findViewById(R.id.theme6);
        theme7 = (RelativeLayout) findViewById(R.id.theme7);
        theme8 = (RelativeLayout) findViewById(R.id.theme8);
        theme9 = (RelativeLayout) findViewById(R.id.theme9);
        theme10 = (RelativeLayout) findViewById(R.id.theme10);
        theme11 = (RelativeLayout) findViewById(R.id.theme11);
        theme12 = (RelativeLayout) findViewById(R.id.theme12);
        theme13 = (RelativeLayout) findViewById(R.id.theme13);
        theme14 = (RelativeLayout) findViewById(R.id.theme14);
        theme15 = (RelativeLayout) findViewById(R.id.theme15);
        theme16 = (RelativeLayout) findViewById(R.id.theme16);
        theme17 = (RelativeLayout) findViewById(R.id.theme17);

        theme1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#f7412c");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f7412c")));
            }
        });

        theme2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#ec1562");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ec1562")));
            }
        });

        theme3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#9e1cb4");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#9e1cb4")));
            }
        });

        theme4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#6733bb");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#6733bb")));
            }
        });

        theme5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#3d4eb8");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3d4eb8")));
            }
        });

        theme6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#1294f6");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1294f6")));
            }
        });

        theme7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#00bcd7");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00bcd7")));
            }
        });

        theme8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#009788");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#009788")));
            }
        });

        theme9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#48b14c");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#48b14c")));
            }
        });

        theme10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#89c541");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#89c541")));
            }
        });

        theme11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#fec200");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#fec200")));
            }
        });

        theme12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#ff9900");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ff9900")));
            }
        });

        theme13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#ff5608");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ff5608")));
            }
        });

        theme14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#7b5548");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#7b5548")));
            }
        });

        theme15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#607d8d");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#607d8d")));
            }
        });

        theme16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#383737");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f7412c")));
            }
        });

        theme17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = Setting.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
                editor.putString("Color", "#000000");
                editor.commit();
                Intent intent = new Intent(Setting.this, Setting.class);
                startActivity(intent);
                overridePendingTransition(0, 0);

                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
            }
        });

    }

    private void addDrawerItems() {

        String[] ArrayData = {getString(R.string.menuHome), getString(R.string.menuCategories),getString(R.string.menuLatestVideo),getString(R.string.menuFavorite),
                getString(R.string.menuTrending), getString(R.string.menuMostLiked), getString(R.string.menuMostShared),getString(R.string.menuAboutUs), getString(R.string.menuTheme),
                getString(R.string.menuShareApp), getString(R.string.menuMoreApp), getString(R.string.menuRateApp)};

        LazyAdapter1 lazy1 = new LazyAdapter1(Setting.this, ArrayData, ArrayImage);
        mDrawerList.setAdapter(lazy1);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(MainActivity.this, "Time for an upgrade!", Toast.LENGTH_SHORT).show();

                Log.d("position", "" + position);
                if (position == 0) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 1) {
                    Intent i = new Intent(getApplicationContext(), Categories.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 2) {
                    Intent i = new Intent(getApplicationContext(), LatestVideoActivity.class);
                    i.putExtra("Type", "LatestVideo");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 3) {
                    Intent i = new Intent(getApplicationContext(), FavouriteActivity.class);
                    i.putExtra("Type", "Favourite");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 4) {
                    Intent i = new Intent(getApplicationContext(), MostLike.class);
                    i.putExtra("Type", "View");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 5) {
                    Intent i = new Intent(getApplicationContext(), MostLike.class);
                    i.putExtra("Type", "Like");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 6) {
                    Intent i = new Intent(getApplicationContext(), MostLike.class);
                    i.putExtra("Type", "Share");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 7) {

                    Intent aboutIntent = new Intent(getApplicationContext(), aboutus.class);
                    startActivity(aboutIntent);

//                    Intent i = new Intent(MostLike.this,UploadVideo.class);
//                    startActivity(i);
//                    overridePendingTransition(0, 0);
//                    finish();

                } else if (position == 8) {

                    Intent i = new Intent(getApplicationContext(), Setting.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();

                } else if (position == 9) {

                    mDrawerLayout.closeDrawers();

                    final RelativeLayout rl_dialoguser = (RelativeLayout) findViewById(R.id.rl_infodialog);
                    rl_dialoguser.setVisibility(View.VISIBLE);
                    layout12 = getLayoutInflater().inflate(R.layout.sharefriend, rl_dialoguser, false);

                    rl_dialoguser.addView(layout12);

                    Button btnFacebook = (Button) layout12.findViewById(R.id.btnFacebook);
                    Button btnEmail = (Button) layout12.findViewById(R.id.btnEmail);
                    Button btnMessage = (Button) layout12.findViewById(R.id.btnMessage);
                    Button btnTwitter = (Button) layout12.findViewById(R.id.btnTwitter);
                    Button btnWhatsapp = (Button) layout12.findViewById(R.id.btnWhatsapp);
                    Button btnExit = (Button) layout12.findViewById(R.id.btnExit);

                    btnFacebook.setTransformationMethod(null);
                    btnEmail.setTransformationMethod(null);
                    btnMessage.setTransformationMethod(null);
                    btnTwitter.setTransformationMethod(null);
                    btnWhatsapp.setTransformationMethod(null);
                    btnExit.setTransformationMethod(null);

                    GradientDrawable gdShare = new GradientDrawable();
                    GradientDrawable gdExit = new GradientDrawable();

                    if (myTheme != null) {

                        gdShare.setShape(GradientDrawable.RECTANGLE);
                        gdShare.setColor(Color.parseColor(myTheme));
                        gdShare.setStroke(1, Color.WHITE);
                        gdShare.setCornerRadius(1.0f);

                        gdExit.setShape(GradientDrawable.RECTANGLE);
                        gdExit.setColor(Color.WHITE);
                        gdExit.setStroke(1, Color.parseColor(myTheme));
                        gdExit.setCornerRadius(5.0f);

                        btnExit.setTextColor(Color.parseColor(myTheme));

                    } else {

                        gdShare.setShape(GradientDrawable.RECTANGLE);
                        gdShare.setColor(Color.parseColor("#f7412c"));
                        gdShare.setStroke(1, Color.WHITE);
                        gdShare.setCornerRadius(1.0f);

                        gdExit.setShape(GradientDrawable.RECTANGLE);
                        gdExit.setColor(Color.WHITE);
                        gdExit.setStroke(1, Color.parseColor("#f7412c"));
                        gdExit.setCornerRadius(5.0f);

                    }

                    btnFacebook.setBackground(gdShare);
                    btnEmail.setBackground(gdShare);
                    btnMessage.setBackground(gdShare);
                    btnTwitter.setBackground(gdShare);
                    btnWhatsapp.setBackground(gdShare);

                    btnExit.setBackground(gdExit);

                    btnFacebook.setTypeface(tf);
                    btnEmail.setTypeface(tf);
                    btnMessage.setTypeface(tf);
                    btnTwitter.setTypeface(tf);
                    btnWhatsapp.setTypeface(tf);
                    btnExit.setTypeface(tf);

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnFacebook.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.SHARE_APP_LINK) + getApplicationContext().getPackageName());
                            // See if official Facebook app is found
                            boolean facebookAppFound = false;
                            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
                            for (ResolveInfo info : matches) {
                                if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook")) {
                                    intent.setPackage(info.activityInfo.packageName);
                                    facebookAppFound = true;
                                    break;
                                }
                            }
                            // As fallback, launch sharer.php in a browser
                            if (!facebookAppFound) {
                                String sharerUrl = getResources().getString(R.string.facebookSharerUrl) + getString(R.string.SHARE_APP_LINK) + getApplicationContext().getPackageName();
                                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                            }
                            startActivity(intent);

                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnEmail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            try {

                                Intent gmail = new Intent(Intent.ACTION_VIEW);
                                gmail.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                                gmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                                gmail.setData(Uri.parse(""));
                                gmail.putExtra(Intent.EXTRA_SUBJECT, "Best App for Odia Video Songs:");
                                gmail.setType("text/plain");
                                gmail.setType("image/jpeg");
                                String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher), "title", null);
                                Uri bmpUri = Uri.parse(pathofBmp);
                                gmail.putExtra(Intent.EXTRA_STREAM, bmpUri);
                                gmail.putExtra(Intent.EXTRA_TEXT, "" + getString(R.string.SHARE_APP_LINK) + getApplicationContext().getPackageName());
                                startActivity(gmail);

                            } catch (Exception e) {

                                sendEmail();

                            }

                            //rl_dialoguser.setVisibility(View.GONE);
                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnTwitter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            try {

                                ImageView imageView1 = new ImageView(getApplicationContext());
                                imageView1.setImageResource(R.drawable.ic_launcher);
                                imageView1.setVisibility(View.GONE);

                                Uri bmpUri = getLocalBitmapUri(imageView1);
//
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_SEND);
                                intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.SHARE_APP_LINK) + getApplicationContext().getPackageName());
                                intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                                intent.setType("text/plain");
                                intent.setType("image/*");
                                intent.setPackage("com.twitter.android");
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
//
                                Toast.makeText(getApplicationContext(), "Twitter not install", Toast.LENGTH_LONG).show();
                            }

                            //rl_dialoguser.setVisibility(View.GONE);
                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //Share Image and Text

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
                            {
                                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getApplication()); // Need to change the build to API 19

                                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                                sendIntent.setType("vnd.android-dir/mms-sms");
                                sendIntent.putExtra("sms_body", getString(R.string.SHARE_APP_LINK));
                                sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher), "title", null)));
                                sendIntent.setType("image/png");

                                if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
                                // any app that support this intent.
                                {
                                    sendIntent.setPackage(defaultSmsPackageName);
                                }
                                startActivity(sendIntent);

                            } else {
                                Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("sms_body", getString(R.string.SHARE_APP_LINK));
                                smsIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher), "title", null)));
                                smsIntent.setType("image/png");
                                startActivity(smsIntent);
                            }

                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnWhatsapp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                String urlToShare = "Best App for Odia Video Songs::" + "\n" + getString(R.string.SHARE_APP_LINK) + getApplicationContext().getPackageName() + "\n";

                                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                                whatsappIntent.putExtra(Intent.EXTRA_TEXT, urlToShare);
                                whatsappIntent.setType("text/plain");
                                whatsappIntent.setPackage("com.whatsapp");
                                startActivity(whatsappIntent);
                                // Detailpage.this.startActivity(whatsappIntent);

                            } catch (ActivityNotFoundException ex) {
                                Toast.makeText(getApplicationContext(), "Whatsapp have not been installed.", Toast.LENGTH_LONG)
                                        .show();
                            }

                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                } else if (position == 10) {
                    Uri uri1 = Uri.parse(getString(R.string.MORE_APP_LINK));
                    Intent iv1 = new Intent(Intent.ACTION_VIEW, uri1);
                    startActivity(iv1);

                } else if (position == 11) {
                    Uri uri = Uri.parse(getString(R.string.RATE_APP_LINK) + getApplicationContext().getPackageName());
                    Intent iv = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(iv);
                }

            }
        });
    }

    private void sendEmail() {
        // TODO Auto-generated method stub
        String recipient = "";
        String subject = "WALLPAPER TEMPLATE";
        @SuppressWarnings("unused")
        String body = "";

        String[] recipients = {recipient};
        Intent email = new Intent(Intent.ACTION_SEND);

        email.setType("message/rfc822");

        email.putExtra(Intent.EXTRA_EMAIL, getString(R.string.SHARE_APP_LINK) + Setting.this.getPackageName());
        email.putExtra(Intent.EXTRA_SUBJECT, subject);

        try {

            startActivity(Intent.createChooser(email, ""));

        } catch (android.content.ActivityNotFoundException ex) {

            //Toast.makeText(Setting.this, getString(R.string.email_no_client), Toast.LENGTH_LONG).show();

        }
    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            // Use methods on Context to access package-specific directories on
            // external storage.
            // This way, you don't need to request external read/write
            // permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
       /* AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id) {
                        Setting.this.finishAffinity();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();*/
        MainActivity.showDialog(this);
        //	super.onBackPressed();

    }

    public class LazyAdapter1 extends BaseAdapter {

        private Activity activity;
        private String[] data;
        private int[] Image;
        private LayoutInflater inflater = null;

        public LazyAdapter1(Activity guide_Fragment, String[] d, int[] image) {
            activity = guide_Fragment;
            data = d;
            Image = image;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.drawercell, null);
            }

            ColorFilter cf;

            RelativeLayout relMidleLine = (RelativeLayout) vi.findViewById(R.id.relMidleLine);

            TextView topic_name = (TextView) vi.findViewById(R.id.txtDrawer);
            topic_name.setText(data[position]);
            topic_name.setTypeface(tf);

            ImageView imageView = (ImageView) vi.findViewById(R.id.imageView);
            imageView.setImageResource(Image[position]);

            if (myTheme != null) {

                cf = new PorterDuffColorFilter(Color.parseColor(myTheme), PorterDuff.Mode.SRC_IN);
                imageView.setColorFilter(cf);

                relMidleLine.setBackgroundColor(Color.parseColor(myTheme));

            } else {
                cf = new PorterDuffColorFilter(Color.parseColor("#f7412c"), PorterDuff.Mode.SRC_IN);
                imageView.setColorFilter(cf);
                relMidleLine.setBackgroundColor(Color.parseColor("#f7412c"));
            }

            return vi;
        }
    }
}
