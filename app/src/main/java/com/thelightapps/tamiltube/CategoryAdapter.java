package com.thelightapps.tamiltube;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by First on 21/07/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    Context context;
    ArrayList<VideoGeterSeter> categoryArrayList;

    public CategoryAdapter(Context context, ArrayList<VideoGeterSeter> category) {
        this.categoryArrayList = category;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_single, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        VideoGeterSeter category = categoryArrayList.get(position);

        holder.txtVideo.setText(category.getName());
        String strThumImage = category.getYtcode();
        Log.d("strThumImage",""+strThumImage);

        strThumImage = context.getResources().getString(R.string.thumbImageBaseUrl)+strThumImage+"/0.jpg";
        Glide.with(context)
                .load(strThumImage)
                .placeholder(R.drawable.loading )
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.grid_image);
    }

    @Override
    public int getItemCount() {
        return categoryArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtVideo;
        ImageView grid_image;
        public MyViewHolder(View itemView) {
            super(itemView);
             txtVideo = (TextView) itemView.findViewById(R.id.txtVideo);
             grid_image = (ImageView) itemView.findViewById(R.id.grid_image);
        }
    }
}