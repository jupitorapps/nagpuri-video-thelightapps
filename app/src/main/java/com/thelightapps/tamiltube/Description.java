package com.thelightapps.tamiltube;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class Description extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, SwipeRefreshLayout.OnRefreshListener {

    String strId, strVideo, strCode, ArraySize, strDescription, strThumImage, strName, myTheme;
    int myPosition;
    TextView txtHeader;
    String cat_id;
    Button btnBack;
    ImageView favourite;
    //ImageView image,btnPlay;
    WebView webViewIngredients;
    SuperRecyclerView recyclerView;
    RelativeLayout relHeader;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private MyPlaybackEventListener playbackEventListener;
    ProgressDialog progressDialog;
    ArrayList<VideoGeterSeter> arrVideoList;
    ArrayList<CategoryGeterSeter> arrCategoryList;

    RealmController realmController;

    //String developer_key = getString(R.string.youTubeAPIKey);
    private YouTubePlayerView youTubeView;
    boolean b = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        SharedPreferences prefs = getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE);
        myTheme = prefs.getString("Color", null);

        setContentView(R.layout.activity_description);

        relHeader = (RelativeLayout) findViewById(R.id.relHeader);

        if (myTheme != null) {

            relHeader.setBackgroundColor(Color.parseColor(myTheme));

        } else {

        }

        txtHeader = (TextView) findViewById(R.id.txtHeader);
        favourite = (ImageView) findViewById(R.id.favourite_IB);
        btnBack = (Button) findViewById(R.id.btnBack);


        /*image = (ImageView) findViewById(R.id.image);
        btnPlay = (ImageView) findViewById(R.id.btnPlay);
        btnPlay.setImageResource(R.drawable.play);*/

        /*ColorFilter cf = new PorterDuffColorFilter(Color.parseColor("#f7412c"), PorterDuff.Mode.SRC_IN);
        btnPlay.setColorFilter(cf);*/


        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(getResources().getString(R.string.youTubeAPIKey), this);

        playbackEventListener = new MyPlaybackEventListener();

        webViewIngredients = (WebView) findViewById(R.id.webViewIngredients);
        recyclerView = (SuperRecyclerView) findViewById(R.id.recyclerview);
        arrVideoList = new ArrayList<VideoGeterSeter>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setRefreshListener(this);
        recyclerView.setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);

        realmController = new RealmController(getApplication());

        Intent iv = getIntent();
        strId = iv.getStringExtra("Id");
        Log.d("DtId", "" + strId);
        strVideo = iv.getStringExtra("Video");
        Log.d("strVideo", "" + strVideo);
        strCode = iv.getStringExtra("Code");
        Log.d("Code", "" + strCode);
        ArraySize = iv.getStringExtra("ArraySize");
        Log.d("ArraySize", "" + ArraySize);
        String Position = iv.getStringExtra("Position");
        Log.d("Position", "" + Position);
        strDescription = iv.getStringExtra("Description");
        Log.d("Description", "" + strDescription);
        strName = iv.getStringExtra("Name");
        cat_id = iv.getStringExtra("cat_id");
        Log.d("Name", "" + strName);

        try {
            myPosition = Integer.parseInt(Position);
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }

        txtHeader.setText(strName);

        //Arabic and English Language Display
        //String str = "للسيطرة عل. عدد ماذا تسبب لأداء بـ.";
        String plain = Html.fromHtml(strDescription).toString();
        webViewIngredients.getSettings().setJavaScriptEnabled(true);
        //webViewIngredients.loadData(""+ Html.fromHtml(strDescription),"text/html", "UTF-8");
        webViewIngredients.loadData(plain, "text/html; charset=UTF-8", null);

        strThumImage = getResources().getString(R.string.thumbImageBaseUrl) + strCode + "/0.jpg";
        setRecyclerview();
        /*Picasso.with(Description.this)
                .load(strThumImage)
                .placeholder(R.drawable.defalt_img )
                .into(image);*/

       /* btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent iv = new Intent(Description.this, VideoDetail.class);
                iv.putExtra("Id", strId);
                iv.putExtra("Position",""+myPosition);
                iv.putExtra("Code",strCode);
                iv.putExtra("Video", strVideo);
                iv.putExtra("ArraySize",ArraySize);
                iv.putExtra("Name",strName);

                startActivity(iv);

            }
        });*/



        recyclerView.addOnItemTouchListener(new SupreRecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                VideoGeterSeter v = arrVideoList.get(position);
                Intent iv = new Intent(Description.this, Description.class);
                iv.putExtra("Id", v.getId());
                iv.putExtra("Code", "" + v.getYtcode());
                iv.putExtra("Video", v.getVideo());
                iv.putExtra("Description", v.getDescription());
                iv.putExtra("Name", v.getName());
                iv.putExtra("ArraySize", "" + arrVideoList.size());
                iv.putExtra("cat_id", "" + v.getCat_id());

                startActivity(iv);
                finish();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        try {
            VideoGeterSeter v = realmController.getVideoByid(strId);
            if (v != null){
                favourite.setImageResource(R.drawable.ic_favorite_black);
                b = false;
            } else {
                favourite.setImageDrawable(getDrawable(R.drawable.ic_favorite_border_white_24dp));
                b=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (b) {
                    favourite.setImageDrawable(getDrawable(R.drawable.ic_favorite_black));
                    realmController.add(strId, strName, strVideo, strCode, strDescription, cat_id);
                    Toast.makeText(Description.this, R.string.addedToFavorite, Toast.LENGTH_SHORT).show();
                    b = false;
                } else {
                    realmController.deleteById(strId);
                    favourite.setImageDrawable(getDrawable(R.drawable.ic_favorite_border_white_24dp));
                }
            }
        });
    }

    CategoryAdapter category;

    private void setRecyclerview() {
        new getCategoryDetailImage().execute();
    }

    int vidPos = 0;

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.setPlaybackEventListener(playbackEventListener);
            //youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            /*Log.d("videocode",""+strCode);
            youTubePlayer.cueVideo(strCode);
            // Hiding player controls
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);*/

            youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                @Override
                public void onLoading() {

                }

                @Override
                public void onLoaded(String s) {

                }

                @Override
                public void onAdStarted() {

                }

                @Override
                public void onVideoStarted() {

                }

                @Override
                public void onVideoEnded() {
                    try {
                        if (strCode.equals(arrVideoList.get(vidPos).getYtcode())) {
                            vidPos++;
                            youTubePlayer.cueVideo(arrVideoList.get(vidPos).getYtcode());
                            vidPos++;
                        } else {
                            youTubePlayer.cueVideo(arrVideoList.get(vidPos).getYtcode());
                            vidPos++;
                        }
                        youTubePlayer.play();
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onError(YouTubePlayer.ErrorReason errorReason) {

                }
            });

            youTubePlayer.cueVideo(strCode); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo


        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    "There was an error initializing youtube player", youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(getResources().getString(R.string.youTubeAPIKey), this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }

    @Override
    public void onRefresh() {
        setRecyclerview();
    }

    public void viewClick(View view) {
        finish();
    }

    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

        @Override
        public void onPlaying() {
            // Called when playback starts, either due to user action or call to play().
            //showMessage("Playing");

            relHeader.setVisibility(View.GONE);

        }

        @Override
        public void onPaused() {
            // Called when playback is paused, either due to user action or call to pause().
            //showMessage("Paused");

            relHeader.setVisibility(View.VISIBLE);

        }

        @Override
        public void onStopped() {
            // Called when playback stops for a reason other than being paused.
            //showMessage("Stopped");
        }

        @Override
        public void onBuffering(boolean b) {
            // Called when buffering starts or ends.
        }

        @Override
        public void onSeekTo(int i) {
            // Called when a jump in playback position occurs, either
            // due to user scrubbing or call to seekRelativeMillis() or seekToMillis()
        }
    }

    public class getCategoryDetailImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getdetailforNearMe();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            category = new CategoryAdapter(getApplicationContext(), arrVideoList);
            recyclerView.setAdapter(category);
        }
    }

    private void getdetailforNearMe() {

        URL hp = null;
        try {

            hp = new URL(getString(R.string.COMMON_URL) + getResources().getString(R.string.getVideoByidUrl) + cat_id);

            Log.d("hp", "" + hp);

            URLConnection hpCon = hp.openConnection();
            hpCon.connect();
            InputStream input = hpCon.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(input));
            String x = "";
            x = r.readLine();
            String total = "";

            while (x != null) {
                total += x;
                x = r.readLine();
            }
            JSONObject jObject = new JSONObject(total);
            Log.d("Object", "" + jObject);

            JSONArray j = jObject.getJSONArray("List_Video");
            for (int i = 0; i < j.length(); i++) {
                JSONObject Obj;
                Obj = j.getJSONObject(i);

                Log.d("Object1", "" + Obj);

                VideoGeterSeter temp = new VideoGeterSeter();

                temp.setId(Obj.getString("id"));
                temp.setVideo(Obj.getString("video"));
                temp.setYtcode(Obj.getString("ytcode"));
                temp.setName(Obj.getString("name"));
                temp.setDescription(Obj.getString("description"));
                temp.setCat_id(Obj.getString("cat_id"));
                Log.d("id", "" + Obj.getString("id"));

                arrVideoList.add(temp);

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
    }
}
