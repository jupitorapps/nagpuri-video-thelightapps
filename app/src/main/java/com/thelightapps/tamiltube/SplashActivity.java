package com.thelightapps.tamiltube;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class SplashActivity extends AppCompatActivity {

//    ImageView imageView;
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        imageView = (ImageView)(findViewById(R.id.splash_IV));
//        GlideDrawableImageViewTarget imagePreview = new GlideDrawableImageViewTarget((ImageView)(findViewById(R.id.splash_IV)));
        Glide.with(this)
                .load(R.mipmap.splash2)
                .asGif()
                .into((ImageView)(findViewById(R.id.splash_IV)));

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                Intent mainIntent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
