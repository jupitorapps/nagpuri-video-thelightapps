package com.thelightapps.tamiltube;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by First on 06-08-2017.
 */


    public class LoadImage extends AsyncTask<Void, Void, String> {
    Activity context;
        String strUrl= "";

    public LoadImage(Activity con){
        this.context = con;

    }
        @Override
        protected String doInBackground(Void... voids) {

            URL hp = null;
            try {

                hp = new URL(context.getResources().getString(R.string.addLinkUrl));

                Log.d("hp", "" + hp);

                URLConnection hpCon = hp.openConnection();
                hpCon.connect();
                InputStream input = hpCon.getInputStream();
                BufferedReader r = new BufferedReader(new InputStreamReader(input));
                String x = "";
                x = r.readLine();
                String total = "";

                while (x != null) {
                    total += x;
                    x = r.readLine();
                }

                JSONObject jObject = new JSONObject(total);
                Log.d("Object", "" + jObject);

                strUrl = jObject.getString("playstoreadlink");

            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (JSONException e1) {
                e1.printStackTrace();
            } catch (NullPointerException e) {
            }

            return strUrl;
        }

        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            SharedPreferences sharedPreferences = context.getSharedPreferences("shared",Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("link",strUrl);
            editor.commit();
           
        }
    }

